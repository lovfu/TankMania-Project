/*
 *	TankMania/ClientData.java
 *
 * 	TankMania/ClientData.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

public class ClientData
{
	public static Tank tank[]= new Tank[100];
	public static Bullet bullet[] = new Bullet[100];
	public static Chat chat[]= new Chat[25];
	public static String myName;
	public static int chatList[] = new int[25];
	public static String chatInfo[] = new String[27];
	public static int chatListIndex[] = new int[25];
	public static int myID;
	
	
	public ClientData()
	{
		int i= 0;
		for(i = 0;i < 100;i++)
			tank[i] = new Tank();
		for(i = 0;i < 100;i++)
			bullet[i] = new Bullet();
		for(i = 0;i < 25 ;i++)
		{
			chat[i] = new Chat();
			chatList[i] = -1;
			chatListIndex[i] = -1;
		}
		chatInfo[0] = new String("Welcome To The Tank Mania World!");
	}
	public void destroyData()
	{
		tank = null;
		chat = null;
		bullet = null;
		myName = null;
		chatList = null;
		chatInfo = null;
		chatListIndex = null;
		myID = -1;
	}
	public static void destroyGame(int id)
	{
		destroyChat(id);
		chat[id].isGame = false;
		bullet = new Bullet[100];
	}
	public static void destroyBullet(int id)
	{
		for(int i = 0;i < 100;i++)
			bullet[i].destroy();
	}	
	public static void updateWait()
	{
		int i ;
		for(i = 0;i <25 ;i++)
			if(!chat[i].isChat)
			{
				if(chatListIndex[i] != -1)
					chatInfo[chatListIndex[i]+2] = "";
				chatListIndex[i] = -1;
			}
		for(i = 0;i < 25;i++)
			if(chatList[i] != -1 &&( !chat[chatList[i]].isChat || chat[chatList[i]].isGame))	//clean the unIsChat ChatID in the List
			{
				if(chatListIndex[i]!=-1)
					chatInfo[chatListIndex[i]+2] ="";
				chatList[i] = -1;
			}
		for(i = 0;i < 25;i++)
			if(chat[i].isChat && !chat[i].isGame && chatListIndex[i]==	-1)
			{
				int j = 0;
				while(chatList[j] != -1)	j++;
				chatList[j] = i;
				chatListIndex[i] = j;
				chatInfo[j+2] = new String(tank[chat[i].ownerID].name+"           "+chat[i].tankNum + "/" + 4);
			}
		for(int j = 0;j < 25;j++)
			if(chatList[j]!=-1)
				chatInfo[j+2] = new String(tank[chat[chatList[j]].ownerID].name+"           "+chat[chatList[j]].tankNum + "/" + 4);
	}
	
	public static void createChat(int chatid,int tankid,String tankname)
	{
		chat[chatid].ownerID = tankid;
		chat[chatid].tankNum = 1;
		chat[chatid].isChat = true;
		chat[chatid].isGame = false;
		chat[chatid].tankID[0]=tankid;
		tank[tankid].chatID = chatid;
		tank[tankid].name = tankname;
		tank[tankid].chatIndex = 0;	

		chatListIndex[chatid] = -1;
		updateWait();
	}	
	public static void destroyChat(int chatid)
	{
		int tankid = chat[chatid].ownerID;
		tank[tankid].chatID = -1;
		tank[tankid].chatIndex = -1;
		
		chat[chatid].tankNum = 0;
		chat[chatid].isGame = false;
		chat[chatid].isChat = false;
		for(int i=0;i<4;i++) chat[chatid].tankID[i] = -1;
		chat[chatid].ownerID  = -1;
		
		chatInfo[chatListIndex[chatid]+2] ="";
		updateWait();
	}
	public static void destroyChater(int chatid,int tankid)
	{
		if(chatid!=-1)
		{	
			chat[chatid].tankNum --;
			if(chat[chatid].tankNum != 0)
				chat[chatid].tankID[tank[tankid].chatIndex] = chat[chatid].tankID[chat[chatid].tankNum];
			chat[chatid].tankID[chat[chatid].tankNum] = -1;
			tank[tankid].chatID = -1;
			tank[tankid].chatIndex = -1;
			updateWait();
		}
	}
	public static void createChater(int chatid,int tankid,String tankname)
	{
		tank[tankid].chatID = chatid;
		tank[tankid].name = new String(tankname);
		tank[tankid].chatIndex = chat[chatid].tankNum;
		chat[chatid].tankID[chat[chatid].tankNum] =tankid;
		chat[chatid].tankNum ++;
		updateWait();
	}
	public static void updateChat(int chatid,int tankid,int chatindex,String str)
	{
		if(chatindex == 0) chat[chatid].ownerID = tankid;
		chat[chatid].tankID[chatindex] = tankid;
		chat[chatid].isChat = true;
		chat[chatid].isGame = true;
		tank[tankid].name = str;
		tank[tankid].chatID = chatid;
		tank[tankid].chatIndex = chatindex;
	}
}
