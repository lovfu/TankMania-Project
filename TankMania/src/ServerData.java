/*
 *	TankMania/ServerData.java
 *
 *  TankMania/ServerData.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

public class ServerData
{
	public static Chat chat[];
	public static Tank tank[];
	public static Bullet bullet[][] = new Bullet[25][100];
	private static int i;
	private static int j;
	public static ServerData data;
	public ServerData()
	{
		chat = new Chat[25];
		tank = new Tank[100];
		for(i = 0;i < 25;i++)
			chat[i] = new Chat();
		for(i = 0;i < 100;i++)
			tank[i] = new Tank();
		for(i = 0;i < 25 ;i++)
			for(j = 0;j < 100;j++)
				bullet[i][j] = new Bullet();
	}
	public static void updateGame(int id)
	{
		for(i = 0;i < 100;i++)
			bullet[id][i].step(i);
		for(i = 0;i < 100;i++)
			bullet[id][i].hit(data,id,i);
		for(i = 0;i < 100;i++)
			tank[i].step(data,i);
	}
	public static void destroyBullet(int id)
	{
		for(i = 0;i < 100;i++)
			bullet[id][i].destroy();
	}
	public static int getFirstChat()
	{
		for(i = 0;i < 25;i++)
		{
			if(!chat[i].isChat)
			{
				return i;
			}
		}
		return -1;
	}
	public static int getFirstBullet(int id)
	{
		for(i = 0;i < 100;i++)
			if(!bullet[id][i].isShow)
				return i;
		return -1;
	}
	public static void initGame(int chatid)
	{
		Tank temp = tank[chat[chatid].tankID[0]];
		temp.x = 300;
		temp.y = 20;
		temp.angle = 0;
		temp.gunAngle = 0;
		temp.life = temp.fullLife;
		if(chat[chatid].tankNum > 1)
		{
			temp = tank[chat[chatid].tankID[1]];
			temp.x = 20;
			temp.y = 200;
			temp.angle = 90;
			temp.gunAngle = 90;
			temp.life = temp.fullLife;
		}
		if(chat[chatid].tankNum > 2)
		{
			temp = tank[chat[chatid].tankID[2]];
			temp.x = 300;
			temp.y = 380;
			temp.angle = 180;
			temp.gunAngle = 180;
			temp.life = temp.fullLife;
		}
		if(chat[chatid].tankNum > 3)
		{
			temp = tank[chat[chatid].tankID[3]];
			temp.x = 580;
			temp.y = 200;
			temp.angle = 270;
			temp.gunAngle = 270;
			temp.life = temp.fullLife;
		}
	}
	public static void leaveChat(int chatid,int tankid)
	{
		if(chat[chatid].ownerID == tankid)
		{
			tank[tankid].chatID = -1;
			tank[tankid].chatIndex = -1;
			chat[chatid].tankNum = 0;
			chat[chatid].isGame = false;
			chat[chatid].isChat = false;
			for(int i=0;i<4;i++) chat[chatid].tankID[i] = -1;
			chat[chatid].ownerID  = -1;
		}
		else
		{
			tank[tankid].chatID = -1;
			tank[tankid].chatIndex = -1;
			chat[chatid].tankNum -= 1;
			chat[chatid].tankID[chat[chatid].tankNum] = -1;
		}
	}
	public static void leaveGame(int chatid,int tankid)
	{
		if(chat[chatid].tankNum ==1 )
		{
			tank[tankid].chatID = -1;
			tank[tankid].chatIndex = -1;
			chat[chatid].tankNum = 0;
			chat[chatid].isGame = false;
			chat[chatid].isChat = false;
			for(int i=0;i<4;i++) chat[chatid].tankID[i] = -1;
			chat[chatid].ownerID  = -1;
		}
		else
		{
			tank[tankid].chatID = -1;
			tank[tankid].chatIndex = -1;
			chat[chatid].tankNum -= 1;
			chat[chatid].tankID[chat[chatid].tankNum] = -1;
		}		
	}
	public static void createChat(int chatid,int tankid)
	{
		chat[chatid].ownerID = tankid;
		chat[chatid].tankNum = 1;
		chat[chatid].isChat = true;
		chat[chatid].isGame = false;
		chat[chatid].tankID[0]=tankid;
		tank[tankid].chatIndex = 0;	
		tank[tankid].chatID = chatid;
		tank[tankid].chatID = chatid;
	}
	public static void joinChat(int chatid,int tankid)
	{
		tank[tankid].chatID = chatid;
		tank[tankid].chatIndex = chat[chatid].tankNum;
		chat[chatid].tankID[chat[chatid].tankNum] =tankid;
		chat[chatid].tankNum++;
	}
	public static boolean tankHit(int tankid,int x,int y)
	{
		int chatid = tank[tankid].chatID;
		if(x < 15 || y < 15 || x > 585 || y > 385)
			return false;
		for(int i = 0;i < chat[chatid].tankNum;i++)
		{
			if(chat[chatid].tankID[i]!=tankid)
			{
				Tank temp = tank[chat[chatid].tankID[i]];
				if((temp.x-x)*(temp.x-x)+(temp.y-y)*(temp.y-y)<=1600)
					return false;
			}
		}
		return true;
	}
}
