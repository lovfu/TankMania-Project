/*
 *	TankMania/ClientShowProtocol.java
 *
 *  TankMania/ClientShowProtocol.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 *	ClientSocket
 * 	ClientData
 * 	ClientTime
 * 	ClientWaitShow
 * 	ClientChatShow
 */
import java.util.*;
import java.text.*;
import java.io.*;

public class ClientShowProtocol	
{
	public static int timeStamp;
	public static int time;
	public static int cost;
	public static int tankID;
	public static int chatID;
	public static int chatIndex;
	public static int bulletID;
	public static int bulletOwnerID;
	public static int tankNum;
	public static int tankTotalNum;
	public static int life;	
	public static String tag1 = new String();
	public static String tag2 = new String();
	public static String tankName = new String();
	public static String chatString = new String();
	public static int tankX;
	public static int tankY;
	public static int tankSX;
	public static int tankSY;
	public static int bulletX;
	public static int bulletY;
	public static int bulletSX;
	public static int bulletSY;	
	public static int tankAngle;
	public static int tankSAngle;
	public static int gunAngle;
	public static int gunSAngle;
	public static int bulletAngle;
	public static Date date;
	
	public static ClientSocket socket;
	public static ClientTime clientTime;
	public static ClientData data;
	public static ClientWaitShow waitShow;
	public static ClientChatShow chatShow;
	public static ClientGameShow gameShow;
	public static String str;
	private static int i;
	private static String temp ;
	public static boolean isRecord;
	public static boolean isGame;
	public static File file;
	public static PrintStream output;
	
	public ClientShowProtocol(boolean b)
	{
		waitShow = new ClientWaitShow();
		chatShow = new ClientChatShow();
		isRecord = b;
		isGame = b;
		if(isRecord == true)
		{
			file=new File(System.getProperty("user.dir"),"record.txt");
			try
			{	
				output=new PrintStream(file);
			}catch(Exception e)
			{
				System.out.println("Record FIle Init Error!");
				System.exit(0);
			}
		}
		isRecord = false;
	}
	
	public static void deal(String s)
	{
		str = s;
		if(str.equals(null))
		{
			System.out.println("Read Null");
			System.exit(0);
		}
		
		if(isRecord())
			output.println(str);
			
		timeStamp = Integer.parseInt(dealWithString());
		tag1 = dealWithString();
		if(tag1.equals("sync"))
		{
			time = Integer.parseInt(dealWithString());
			clientTime.update(timeStamp,time);
			return;
		}
		else if(!tag1.equals("move"))
		{
			System.out.println("Recieve: "+tag1+" "+str);
		}
		if(tag1.equals("set-id"))
		{
			tankID = Integer.parseInt(dealWithString());
			data.myID = tankID;
			data.tank[tankID].isConnected = true;
			return ;
		}
		if(tag1.equals("destroy") || tag1.equals("create"))
		{
			tag2 = dealWithString();
			if(tag1.equals("create") && tag2.equals("tank"))
			{		
				tankID = Integer.parseInt(dealWithString());//TankID
				tankName = dealWithString();//TankName
				dealWithString();//TankType
				dealWithString();//GunType
				life = 	Integer.parseInt(dealWithString());//Life
				dealWithString();
				tankX = Integer.parseInt(dealWithString());//X
				tankY = Integer.parseInt(dealWithString());//Y
				tankAngle = Integer.parseInt(dealWithString());//tankAngle
				gunAngle = Integer.parseInt(dealWithString());//gunAngle
				
				if(gameShow != null && gameShow.getOpened())
					data.tank[tankID].set(tankName,tankX,tankY,life,tankAngle,gunAngle);
				return;
			}
			if(tag1.equals("destroy") && tag2.equals("tank"))
			{
				tankID = Integer.parseInt(dealWithString());
				data.destroyChater(data.tank[data.myID].chatID,tankID);
				if(tankID == data.myID && data.chat[chatID].isGame)
				{
					gameShow.close();
					new ClientGameOverShow(isGame);
				}
				return ;
			}
			if(tag1.equals("create") && tag2.equals("bullet"))
			{
				bulletID = Integer.parseInt(dealWithString());
				dealWithString();//Type					
				bulletOwnerID = Integer.parseInt(dealWithString());
				bulletX = Integer.parseInt(dealWithString());
				bulletY = Integer.parseInt(dealWithString());
				dealWithString();
				if(gameShow != null && gameShow.getOpened())
				{
					bulletAngle = data.tank[bulletOwnerID].gunAngle;
					data.bullet[bulletID].set(bulletOwnerID,bulletX,bulletY,bulletAngle);
				}
				return;
			}
			if(tag1.equals("destroy") && tag2.equals("bullet"))
			{
				bulletID = Integer.parseInt(dealWithString());
				if(gameShow != null && gameShow.getOpened())
					data.bullet[bulletID].isShow = false;
				return;
			}			
			if(tag1.equals("create") && tag2.equals("chat"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankID = Integer.parseInt(dealWithString());
				tankName = dealWithString();
				data.createChat(chatID,tankID,tankName);
				return;
			}
			if(tag1.equals("destroy")  && tag2.equals("chat"))
			{
				chatID = Integer.parseInt(dealWithString());
				if(data.tank[data.myID].chatID == chatID )
				{
					chatShow.frame.setVisible(false);
					waitShow.frame.setVisible(true);
				}
				data.destroyChat(chatID);
				return;
			}
			if(tag1.equals("create") && tag2.equals("game"))
			{
				chatID = Integer.parseInt(dealWithString());
				data.chat[chatID].isGame = true;
				if(chatID == data.tank[data.myID].chatID)
				{
					data.destroyBullet(chatID);
					gameShow  = new ClientGameShow();
					chatShow.close();
					gameShow.create();
				}
				return ;
			}
			if(tag1.equals("destroy") && tag2.equals("game"))
			{
				chatID = Integer.parseInt(dealWithString());
				data.destroyChat(chatID);
				return;				
			}
			if(tag1.equals("create") && tag2.equals("chater"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankID = Integer.parseInt(dealWithString());
				tankName = dealWithString();
				data.createChater(chatID,tankID,tankName);
				return;
			}
			if(tag1.equals("destroy") && tag2.equals("chater"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankID = Integer.parseInt(dealWithString());
				data.destroyChater(chatID,tankID);
				if(tankID == data.myID && data.chat[chatID].isGame)
				{
					gameShow.close();
					new ClientGameOverShow(isGame);
				}
				return;		
			}
		}
		
		if(tag1.equals("chat"))
		{
			tag2 = dealWithString();
			if(tag1.equals("chat") && tag2.equals("chat"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankID = Integer.parseInt(dealWithString());
				if(chatShow.getOpened())
				{
					chatString = new String(str);
					date =new Date();
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String currentTime= new String(formatter.format(date));
					chatShow.showString+=currentTime+" "+data.tank[tankID].name+" :\n"+chatString+"\n";
					chatShow.show.setText(chatShow.showString);
					return ;
				}
			}
		}
		if(tag1.equals("game"))
		{
			tag2 = dealWithString();
			if(tag1.equals("game") && tag2.equals("chat"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankID = Integer.parseInt(dealWithString());
				if(gameShow != null && gameShow.getOpened())
				{
					chatString = new String(str);
					data.tank[tankID].update(chatString);
				}
				return ;
			}
		}		
		if(tag1.equals("move"))
		{
			tag2 = dealWithString();
			if(tag1.equals("move") && tag2.equals("tank"))
			{
				tankID = Integer.parseInt(dealWithString());
				tankSX = Integer.parseInt(dealWithString());
				tankSY = Integer.parseInt(dealWithString());
				tankX = Integer.parseInt(dealWithString());
				tankY = Integer.parseInt(dealWithString());
				cost = Integer.parseInt(dealWithString());
				if(gameShow != null && gameShow.getOpened())
					data.tank[tankID].move(tankX,tankY);
				return ;
			}
			if(tag1.equals("move") && tag2.equals("bullet"))
			{
				bulletID = Integer.parseInt(dealWithString());
				bulletSX = Integer.parseInt(dealWithString());
				bulletSY = Integer.parseInt(dealWithString());
				bulletX = Integer.parseInt(dealWithString());
				bulletY = Integer.parseInt(dealWithString());
				cost = Integer.parseInt(dealWithString());	
				if(gameShow != null && gameShow.getOpened())
					data.bullet[bulletID].move(bulletX,bulletY);
				return ;
			}			
		}
		
		if(tag1.equals("rotate-tank"))
		{
			tag2 = dealWithString();
			tankID = Integer.parseInt(dealWithString());
			tankSAngle = Integer.parseInt(dealWithString());
			tankAngle =  Integer.parseInt(dealWithString());
			cost = Integer.parseInt(dealWithString());
			if(gameShow != null && gameShow.getOpened())
				data.tank[tankID].angle = tankAngle;
			return ;
		}
		
		if(tag1.equals("rotate-gun"))
		{
			tag2 = dealWithString();
			tankID = Integer.parseInt(dealWithString());
			gunSAngle = Integer.parseInt(dealWithString());
			gunAngle =  Integer.parseInt(dealWithString());
			cost = Integer.parseInt(dealWithString());
			if(gameShow != null && gameShow.getOpened())
				data.tank[tankID].gunAngle = gunAngle;
			return ;
		}
		
		if(tag1.equals("set-life"))
		{
			tag2 = dealWithString();
			tankID = Integer.parseInt(dealWithString());
			life =  Integer.parseInt(dealWithString());
			if(gameShow != null && gameShow.getOpened())
			{
				data.tank[tankID].life = life;
				data.tank[tankID].isHited = true;
			}
			return ;
		}
		
		if(tag1.equals("update"))
		{
			tag2 = dealWithString();
			if(tag1.equals("update") && tag2.equals("chat"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankNum  = Integer.parseInt(dealWithString());
				data.chat[chatID].tankNum = tankNum;
				return ;
			}
			if(tag1.equals("update") && tag2.equals("chater"))
			{
				chatID = Integer.parseInt(dealWithString());
				tankID = Integer.parseInt(dealWithString());
				chatIndex = Integer.parseInt(dealWithString());
				tankName = dealWithString();
				data.updateChat(chatID,tankID,chatIndex,tankName);
				return;
			}
		}
	}
	
	public static boolean isRecord()
	{
		int chatid = data.tank[data.myID].chatID;
		String strTemp = new String(str);
		if(!isRecord)
		{
			return false;
		}
		else
		{
			dealWithString();
			String tagTemp1 = new String(dealWithString());
			String tagTemp2 = new String(dealWithString());
			if(tag1.equals("set-id"))
			{
				output.println(str);
				return false;
			}
			if(tagTemp1.equals("create")&&tagTemp2.equals("game"))
			{
				chatID = Integer.parseInt(dealWithString());
				if(chatID == chatid)
				{
					isRecord = true;
					str = new String(strTemp);
					try
					{
						file.createNewFile();
					}catch(Exception e)
					{
						System.out.println("New FIle Error!");
						System.exit(0);
					}
					return true;
				}
			}
			if(tagTemp1.equals("destroy")&&tagTemp2.equals("game"))
			{
				chatID = Integer.parseInt(dealWithString());
				if(chatID ==  chatid)
				{
					isRecord = false;
					str = new String(strTemp);
					output.println(str);
					output.println("end");
					return false;
				}
			}
			if(tagTemp1.equals("destroy")&&tagTemp2.equals("tank"))
			{
				tankID = Integer.parseInt(dealWithString());
				if(tankID == data.myID)
				{
					str = new String(strTemp);
					output.println(str);
					output.println("end");
					return false;
				}
			}
			if(tagTemp1.equals("destroy")&&tagTemp2.equals("chater"))
			{
				tankID = Integer.parseInt(dealWithString());
				if(tankID == data.myID)
				{
					isRecord = false;
					str = new String(strTemp);
					output.println(str);
					output.println("end");
					return false;					
				}	
			}	
			str = new String(strTemp);
		}
		return isRecord;
	}
	
	private static String dealWithString()
	{
		i = str.indexOf(32);
		temp = new String(str.substring(0,i));
		str =  new String(str.substring(i+1));
		return temp;
	}
}
