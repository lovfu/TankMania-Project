public class ClientGameOverShow
{
	public static ClientGameShow gameShow;
	public static ClientChatShow chatShow;
	public static ClientWaitShow waitShow;
	public static ClientLogShow logShow;
	public ClientGameOverShow(boolean b)
	{
		gameShow.close();
		if(b)
		{
			if(chatShow.isCheck)
			{
				new ClientRecordSave();
			}
			else
				waitShow.show();
			return;
		}
	}
}
