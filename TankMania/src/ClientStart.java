/*
 *	TankMania/ClientStart.java
 *
 *  TankMania/ClientStart.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*	
 * 	ClientRequestThread
 * 	ClientShowThread
 * 	ClientSocket
 * 	
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.net.*;

public class ClientStart implements ActionListener
{
	private int alpha=240;
	private int n =0;
	private BufferedImage img;
	private JWindow window;
	private JLabel label;
	private Timer time;
	public static ClientRequestThread clientRequest;
	public static ClientShowThread clientShow;
	
	public static void main(String args[])
	{
		new ClientStart();
	}
	public ClientStart()
	{
		ClientSocket.go();						//Socket Connect
		clientRequest = new ClientRequestThread();
		clientShow = new ClientShowThread();
		clientShow.start();						//Show	Thread	start
		clientRequest.start();					//Request Thread Start
		window= new JWindow();
		File sourceimage = new File("Tank.png");
		time = new Timer(5,this);
		try{
			img = ImageIO.read(sourceimage);
		}catch(Exception e){
			System.out.println("Image Load Error!");
			System.exit(0);
		}

		label = new JLabel(new ImageIcon(img));
		window.setSize(img.getWidth(),img.getHeight());
		int x = WindowLocation.getX(img.getWidth());
		int y = WindowLocation.getY(img.getHeight());
		window.setLocation(x, y);
		window.getContentPane().add(label, BorderLayout.CENTER);
		window.pack();
		window.setVisible(true);
		time.start();
	}

	
	public void actionPerformed(ActionEvent e)
	{
		if(n >= 200 && alpha>=0){
			int i,j,rgb;
			for (i = img.getMinX();i < img.getWidth(); i++)
			{
				for (j = img.getMinY(); j < img.getHeight(); j++)
				{
					rgb = img.getRGB(i,j);
					rgb = ( alpha<< 24) | (rgb & 0x00ffffff);
					img.setRGB(i,j,rgb);
				}
			}
			label.setIcon(new ImageIcon(img));
			alpha = alpha - 40;
		}
		else  if(n < 200) n = n + 1;
		else
		{
			window.setVisible(false);

			time.stop();
			new ClientLogShow();					//GUI Part Go On
		}	
	}  	
}
