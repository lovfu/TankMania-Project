/*
 *	TankMania/Tank.java
 * 
 *  TankMania/Tank.class 
 *
 * (C) 王宏伟 Lovfu 2008010486 计84
 * 
 */

public class Tank 
{
	public final int fullLife = 100;
	public int life;
	public final int speed = 2;
	public String name;
	public int chatID;
	public boolean isConnected;
	public boolean isHited;
	public int chatIndex;
	public int x;
	public int y;
	public int angle;
	public int gunAngle;
	public int chatTime;
	public int hitTime;
	public String chatStr;
	public boolean isShow;
	public static ClientTime timer;
	
	public void init(String str)
	{
		name = str;	//	Log Request
		chatID = -1;
		life = fullLife;
		isConnected = true;
	}
	public void move(int x,int y)
	{
		this.x = x;
		this.y = y;
	}
	public void update(String str)
	{
		chatStr = new String(str);
		chatTime = 0;
		isShow = true;
	}
	
	public void isHit()
	{
		isHited = true;
		hitTime = 0;
	}
	public void step(ServerData data,int id)
	{
		if(data.tank[id].chatID != -1 && data.chat[data.tank[id].chatID].isGame)
		{
			int tx = x + (int)((double)speed*Math.cos(angle/180.0*Math.PI));
			int ty = y + (int)((double)speed*Math.sin(angle/180.0*Math.PI));
			if(tx < 5 || ty < 5 || tx > 590 || ty > 390)
				return;
			x = tx;
			y = ty;
		}
	}
	public void hitDie()
	{
		if(hitTime > timer.hitRound/timer.updateRound )
		{
			isHited = false;
			hitTime = 0;
		}
		else
			hitTime ++;		
	}
	public void chatDie()
	{
		if(chatTime > timer.chatRound/timer.updateRound )
		{
			isShow = false;
			chatTime = 0;
			chatStr = "";
		}
		else
			chatTime ++;
	}
		
	public Tank()
	{
		name = new String();
		chatID = -1;
		isConnected = false;
		isHited = false;
		chatIndex = -1;
		chatStr = new String();
		isShow = false;
		chatTime = 0;	
		hitTime = 0;
		life = fullLife;
	}
	public Tank(String str,int x,int y,int angle,int gunAngle)
	{
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.gunAngle = gunAngle;
		name = new String(str);
		isConnected = false;
		isHited = false;
		chatID = -1;
		chatIndex = -1;
		chatStr = new String();
		isShow = false;
		chatTime = 0;	
		hitTime = 0;
	}
	public void set(String str,int x,int y,int life,int angle,int gunAngle)
	{
		this.name = str;
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.gunAngle = gunAngle;
		this.life = life;
	}
}
