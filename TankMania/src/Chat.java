/*
 *	TankMania/Chat.java
 *
 *  TankMania/Chat.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

public class Chat
{
	public int ownerID;
	public int tankNum;
	public int tankID[];
	public boolean isChat;
	public boolean isGame;
	
	public Chat()
	{
		tankNum = 0;
		ownerID = -1;
		tankID = new int[4];
		isGame = false;
		isChat = false;
		for(int i=0;i<4;i++) tankID[i] = -1;
	}
}
