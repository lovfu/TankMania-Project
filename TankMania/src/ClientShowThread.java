/*
 *	TankMania/ClientShowThread.java
 *
 *  TankMania/ClientShowThread.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*	 
 * 	 ClientShowProtocol
 */

public class ClientShowThread extends Thread
{
	public static ClientShowProtocol clientShowProtocol;
	public static ClientSocket socket;
	public ClientShowThread()
	{
		new ClientData();
		clientShowProtocol = new ClientShowProtocol(true);
	}
	public void run()
	{
		String str = null;
		while(true)
		{
			try{
				if(!socket.socket.isClosed() && socket.socket.isConnected())
				  str = new String(socket.is.readLine());
				else return ;
			}
			catch(Exception e) {	
				System.out.println("Read Error!");
				System.exit(0);	
			}
			clientShowProtocol.deal(str);
		}
	}
}
