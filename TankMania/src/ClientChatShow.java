/*
 *	TankMania/ClientChatShow.java
 *
 *  TankMania/ClientChatShow.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 * ClientData
 * 
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class ClientChatShow implements ActionListener
{
	public static JFrame frame;
	public static JButton start;
	public static JButton exit;
	public static JButton enter;
	public static JTextArea show;
	public static JTextField enterText;
	public static JList list;
	public static JScrollPane scrollPane;	
	public static JLabel label;
	public static BufferedImage img;
	public static ClientData data;
	public static JSeparator separatorH;
	public static JSeparator separatorV;
	private static Timer time;
	public static String showString;
	private static Font font1;
	private static Font font2;
	public JCheckBox check;
	public static boolean isCheck;
	
	public ClientChatShow()
	{
		File sourceImage = new File("light.png");
		try{
			img = ImageIO.read(sourceImage);
		}catch(Exception e){
			System.out.println("Error!");
			System.exit(0);
		}
		font1 = new Font("DialogInput",Font.BOLD,18);
		font2 = new Font("DialogInput",Font.BOLD,15);
		time = new Timer(1000,this);
		separatorH = new JSeparator(SwingConstants.VERTICAL);
		separatorV = new JSeparator();
		frame = new JFrame("TankMania-Chating Room");
		start = new JButton("Start");
		exit = new JButton("Leave");
		enter = new JButton("Enter");
		show = new JTextArea();
		enterText = new JTextField();
		list = new JList();
		label = new JLabel(new ImageIcon(img));
		scrollPane = new JScrollPane(show,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		check = new JCheckBox("Record");
		
		frame.setResizable(false);
		frame.setLayout(null);
		frame.getContentPane().add(scrollPane);
		frame.getContentPane().add(start);
		frame.getContentPane().add(exit);
		frame.getContentPane().add(list);
		frame.getContentPane().add(label);
		frame.getContentPane().add(enter);
		frame.getContentPane().add(check);
		frame.getContentPane().add(enterText);
		frame.getContentPane().add(separatorH);
		frame.getContentPane().add(separatorV);
		show.setFont(font1);
		enterText.setFont(font2);
		label.setBounds(0,0,img.getWidth(),img.getHeight());
		int t = WindowLocation.getWidth()*5/8-img.getWidth();
		scrollPane.setBounds(img.getWidth(),0,t*3/4,WindowLocation.getHeight()/2-70);
		separatorV.setBounds(img.getWidth(),WindowLocation.getHeight()/2-75,t,5);
		enterText.setBounds(img.getWidth(),WindowLocation.getHeight()/2-65,t*3/4,30);
		separatorH.setBounds(img.getWidth()+t*3/4,0,5,WindowLocation.getHeight()/2-80);
		list.setBounds(img.getWidth()+t*3/4+5,0,t/4-7,WindowLocation.getHeight()/2-200);
		check.setBounds(img.getWidth()+t*3/4+10,WindowLocation.getHeight()/2-185,90,20);		
		start.setBounds(img.getWidth()+t*3/4+10,WindowLocation.getHeight()/2-155,90,30);
		exit.setBounds(img.getWidth()+t*3/4+10,WindowLocation.getHeight()/2-115,90,30);
		enter.setBounds(img.getWidth()+t*3/4+10,WindowLocation.getHeight()/2-65,90,30);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(false);
		show.setEditable(false);
		frame.setSize(WindowLocation.getWidth()*5/8,WindowLocation.getHeight()/2);
		
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);
		
		frame.addWindowListener(ClientStart.clientRequest);
		exit.addMouseListener(ClientStart.clientRequest);
		start.addMouseListener(ClientStart.clientRequest);
		enter.addMouseListener(ClientStart.clientRequest);
		enterText.addMouseListener(ClientStart.clientRequest);
		exit.addKeyListener(ClientStart.clientRequest);
		start.addKeyListener(ClientStart.clientRequest);
		enter.addKeyListener(ClientStart.clientRequest);
		enterText.addKeyListener(ClientStart.clientRequest);
		check.addMouseListener(ClientStart.clientRequest);
	}
	public static void close()
	{
		frame.setVisible(false);
		show = new JTextArea();
		list = new JList();
		time.stop();
	}
	public static boolean getOpened()
	{
		return frame.isVisible();
	}
	public static void create(boolean b)
	{
		showString="";
		frame.setVisible(true);
		start.setVisible(b);
		show.setText(null);
		time.start();
		enterText.requestFocus();
		isCheck = false;
	}
	public void actionPerformed(ActionEvent e)
	{
		if(data.tank[data.myID].chatID!=-1)
		{
			int t=0;
			String temp[] = new String[4];
			while(t<4 && data.chat[data.tank[data.myID].chatID].tankID[t]!=-1)
			{
				temp[t] = new String(data.tank[data.chat[data.tank[data.myID].chatID].tankID[t]].name);
				t++;
			}
			list.setListData(temp);
		}
			
	}	
}
	
