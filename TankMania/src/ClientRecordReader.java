/*
 *	TankMania/ClientRecordReader.java
 *
 *  TankMania/ClientRecordReader.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */
import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.awt.event.*;

public class ClientRecordReader implements MouseListener,KeyListener
{
	public static JFrame frame;
	public static JTextField reader;
	public static RecordFileReader fileReader;
	public static JLabel label;
	public static JButton enter;
	public static JButton choose;
	public static JButton exit;
	private static Font font;
	
	public ClientRecordReader()
	{
		frame = new JFrame("TankMania Record Reader");
		reader = new JTextField();
		label = new JLabel("Please Choose The Record File");
		exit = new JButton("Exit");
		enter = new JButton("Enter");
		choose = new JButton("Open");
		font = new Font("DialogInput",Font.BOLD,15);
		fileReader = new RecordFileReader();
		frame.getContentPane().add(reader);
		frame.getContentPane().add(label);
		frame.getContentPane().add(choose);
		frame.getContentPane().add(enter);
		frame.getContentPane().add(exit);
		frame.setResizable(false);
		frame.setLayout(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(WindowLocation.getWidth()/3,WindowLocation.getHeight()/3);
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);
		reader.setEditable(false);
		
		label.setFont(font);
		label.setBounds(50,30,300,50);
		reader.setBounds(50,80,300,30);
		choose.setBounds(50,130,80,30);
		enter.setBounds(150,130,80,30);
		exit.setBounds(250,130,80,30);
		
		choose.addMouseListener(this);
		enter.addMouseListener(this);
		exit.addMouseListener(this);
		choose.addKeyListener(this);
		enter.addKeyListener(this);
		exit.addKeyListener(this);
	}
	public void mouseExited(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseEntered(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void keyReleased(KeyEvent e){}
	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == exit)
			System.exit(0);
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == choose)
		{
			frame.setVisible(false);
			fileReader.setVisible(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == enter)
		{
			if(reader.getText().trim().length() > 0)
			{
				new ClientRecordShow(reader.getText());
			}
		}
	}
	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent() == exit)
			System.exit(0);
		if(e.getComponent() == choose)
		{
			frame.setVisible(false);
			fileReader.frame.setVisible(true);
		}
		if(e.getComponent() == enter)
		{
			if(reader.getText().trim().length() > 0)
			{
				new ClientRecordShow(reader.getText());
			}			
		}
	}
	
	public static void main(String args[])
	{
		new ClientRecordReader();
	}
}

class RecordFileReader extends JFileChooser
{
	public JFrame frame;
	public String str;
	public ClientRecordReader reader;
	public RecordFileReader()
	{
		super(System.getProperty("user.dir"));
		frame = new JFrame("TankMania Record Reader");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(false);
		frame.setSize(WindowLocation.getWidth()/2,WindowLocation.getHeight()/2);
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);
		frame.getContentPane().add(this);
	}
	public void approveSelection()
	{
		reader.reader.setText(getSelectedFile().getName());
		frame.setVisible(false);
		reader.frame.setVisible(true);
	}
	public void cancelSelection() 
	{
		frame.setVisible(false);
		reader.frame.setVisible(true);
	}
}
