/*
 *	TankMania/ClientLogShow.java
 *
 *  TankMania/ClientLogShow.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 *	ClientData; 
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.*;
import java.net.*;

public class ClientLogShow
{
	private static JFrame frame;
	public static JButton connectButton;
	public static JButton exitButton;
	private static JTextField enterText;
	private static JLabel infoLabel;
	public static ClientSocket socket;
	public static ClientData data;
	public static ClientRequestThread clientRequest;
	public static ClientShowThread clientShow;	
	
	public ClientLogShow()
	{
		data = new ClientData();
		frame = new JFrame("TankMania");
		connectButton = new JButton("Connect");
		exitButton = new  JButton("Exit");
		enterText = new JTextField();
		infoLabel = new JLabel("Player:");
		
		frame.getContentPane().add(connectButton);
		frame.getContentPane().add(exitButton);
		frame.getContentPane().add(enterText);
		frame.getContentPane().add(infoLabel);

		frame.setResizable(false);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setVisible(true);
		frame.setSize(300,200);
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);

		infoLabel.setBounds(30,20,100,50);
		enterText.setBounds(100,30,150,30);
		connectButton.setBounds(30,100,100,35);
		exitButton.setBounds(170,100,100,35);
		
		frame.addWindowListener(ClientStart.clientRequest);
		connectButton.addMouseListener(ClientStart.clientRequest);
		exitButton.addMouseListener(ClientStart.clientRequest);
		connectButton.addKeyListener(ClientStart.clientRequest);
		exitButton.addKeyListener(ClientStart.clientRequest);
		connectButton.setMnemonic(KeyEvent.VK_ENTER);
	}	
	public static void connect()
	{
		if(enterText.getText().trim().length() > 0)
		{
			socket = new ClientSocket();
			String str = enterText.getText();
			frame.setVisible(false);
			data.myName = new String(str);	//Set Local Client tank Name
			data.chatInfo[1] = new String("I am "+data.myName+"!");
		}
	}
}
