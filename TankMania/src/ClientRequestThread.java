/*
 *	TankMania/ClientRequestThread.java
 *
 *  TankMania/ClientRequestThread.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 *	ClientSocket 
 * 	ClientTime
 * 	ClientLogShow
 * 	ClientWaitShow
 * 	ClientChatShow
 * 	ClientData
 */

import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;

public class ClientRequestThread extends Thread implements KeyListener,MouseListener,WindowListener,ActionListener
{
	private static ClientSocket socket ;
	public static ClientTime time;
	public static ClientLogShow logShow;
	public static ClientWaitShow waitShow;
	public static ClientChatShow chatShow;
	public static ClientGameShow gameShow;
	public static ClientShowProtocol clientShowProtocol;
	public static ClientData data;
	public static Timer timer;
	
	public ClientRequestThread()
	{
		new ClientSocket();
		time = new ClientTime();
		timer = new Timer(5,this);
		timer.start();
	}
	public void actionPerformed(ActionEvent e)
	{
		if(time.getTime() >= time.getLast() + time.getRound())
			socket.output();
	}
	public void mouseExited(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseEntered(MouseEvent e){}
	public void windowActivated(WindowEvent e){}
	public void windowDeactivated(WindowEvent e){}
	public void windowDeiconified(WindowEvent e){}
	public void windowIconified(WindowEvent e){}
	public void windowOpened(WindowEvent e){}
	public void windowClosed(WindowEvent e){}
	public void keyReleased(KeyEvent e){}
	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == logShow.connectButton)
		{
			logShow.connect();
			socket.out("born "+data.myName+" 0 0 0 0 ");
			waitShow = new ClientWaitShow();
			waitShow.show();
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && (e.getSource() == logShow.exitButton || e.getSource() == waitShow.exit))
		{
			socket.exit();
			try
			{
				socket.socket.close();
			}catch(Exception ex)
			{
				System.out.println("Socket Close Error!");
				System.exit(0);
			}			
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == waitShow.list)	
		{
			waitShow.selected = waitShow.list.getSelectedIndex();		
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == waitShow.chat)	
		{
			socket.out("create chat ");
			waitShow.close();	
			chatShow = new ClientChatShow();
			chatShow.create(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == waitShow.join)
		{
			socket.out("join chat "+ data.chatList[waitShow.selected-2]);
			waitShow.frame.setVisible(false);
			chatShow = new ClientChatShow();
			chatShow.create(false);
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && (e.getSource() == chatShow.enter || e.getSource() == chatShow.enterText) && chatShow.enterText.getText().trim().length() > 0)
		{
			socket.out("chat chat "+chatShow.enterText.getText()+" ");
			chatShow.enterText.setText(null);
			chatShow.enterText.requestFocus();			
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && (e.getSource() == gameShow.enter  || e.getSource() == gameShow.enterText) && gameShow.enterText.getText().trim().length() > 0)
		{
			socket.out("game chat "+gameShow.enterText.getText()+" ");
			gameShow.enterText.setText(null);
			gameShow.panel.requestFocus();						
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && (e.getSource() == gameShow.enter  || e.getSource() == gameShow.enterText) && gameShow.enterText.getText().trim().length() == 0)		
		{
			gameShow.panel.requestFocus();	
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == gameShow.panel)
		{
			gameShow.enterText.requestFocus();
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == chatShow.exit)
		{
			socket.out("leave chat ");
			chatShow.close();
			waitShow.show();
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == gameShow.exit)
		{
			socket.out("leave game ");
		}
		if(e.getSource() == gameShow.panel || e.getSource() == gameShow.enter || e.getSource() == gameShow.exit)
		{
			Tank temp = data.tank[data.myID];
			int x,y;
			if(e.getKeyCode() == KeyEvent.VK_W || e.getKeyCode() == KeyEvent.VK_UP)	
			{
				x = temp.x; 
				y =temp.y+40;
				socket.out("move-to "+x+" "+y+" ");
			}
			if(e.getKeyCode()  == KeyEvent.VK_A || e.getKeyCode() == KeyEvent.VK_LEFT)
			{
				x = temp.x-40;
				y = temp.y;
				socket.out("move-to "+x+" "+y+" ");	
			}
			if(e.getKeyCode() == KeyEvent.VK_S || e.getKeyCode()  == KeyEvent.VK_DOWN)
			{
				x = temp.x;
				y = temp.y-40;
				socket.out("move-to "+x+" "+y+" ");					
			}
			if(e.getKeyCode()  == KeyEvent.VK_D || e.getKeyCode()  == KeyEvent.VK_RIGHT)
			{
				x = temp.x+40;
				y = temp.y;
				socket.out("move-to "+x+" "+y+" ");
			}
			if(e.getKeyCode() == KeyEvent.VK_SPACE || e.getKeyCode() == KeyEvent.VK_ALT_GRAPH || e.getKeyCode() == KeyEvent.VK_CONTROL || e.getKeyCode() == KeyEvent.VK_SHIFT)
				socket.out("shoot ");
		}		
	}
	public void windowClosing(WindowEvent e)
	{
		if(e.getSource() == waitShow.frame)
			socket.exit();
		if(e.getSource() == chatShow.frame)
		{
			socket.out("leave chat ");
			socket.exit();
		}
		if(e.getSource() == gameShow.frame)
		{
			socket.out("leave game ");
			socket.exit();
		}
		try
		{
			socket.socket.close();
		}catch(Exception ex)
		{
			System.out.println("Socket Close Error!");
			System.exit(0);
		}
	}	
	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent() == logShow.connectButton)
		{
			logShow.connect();
			socket.out("born "+data.myName+" 0 0 0 0 ");	//Log	Request
			waitShow = new ClientWaitShow();
			waitShow.show();
		}
		if(e.getComponent() == waitShow.chat)
		{
			socket.out("create chat ");
			waitShow.close();
			chatShow = new ClientChatShow();
			chatShow.create(true);
		}
		if(e.getComponent() == waitShow.join)
		{
			if(waitShow.selected!=0 && waitShow.selected!=1)
			{
				socket.out("join chat "+ data.chatList[waitShow.selected-2]+" ");
				data.tank[data.myID].chatID = data.chatList[waitShow.selected-2];
				waitShow.close();
				chatShow = new ClientChatShow();
				chatShow.create(false);
			}
		}
		if(e.getComponent() == waitShow.list)
		{
			waitShow.selected = waitShow.list.getSelectedIndex();
		}
	
		if(e.getComponent() == chatShow.start)
		{
			socket.out("create game ");			
		}
		if(e.getComponent() == chatShow.exit)
		{
			socket.out("leave chat ");
			chatShow.close();
			waitShow.show();
		}
		if(e.getComponent() == chatShow.enterText)
		{
			chatShow.enterText.requestFocus();
		}
		if(e.getComponent() == gameShow.enterText)
		{
			gameShow.enterText.requestFocus();
		}		
		if(e.getComponent() == chatShow.enter && chatShow.enterText.getText().trim().length() > 0)
		{
			socket.out("chat chat "+chatShow.enterText.getText()+" ");
			chatShow.enterText.setText(null);
			chatShow.enterText.requestFocus();
		}
		if(e.getComponent() == logShow.exitButton || e.getComponent() == waitShow.exit)
		{
			socket.exit();
			try
			{
				socket.socket.close();
			}catch(Exception ex)
			{
				System.out.println("Socket Close Error!");
				System.exit(0);
			}			
		}
		if(e.getComponent() == gameShow.exit)
		{
			socket.out("leave game ");
		}
		if(e.getComponent() == gameShow.enter  && gameShow.enterText.getText().trim().length() > 0)
		{
			socket.out("game chat "+gameShow.enterText.getText()+" ");
			gameShow.enterText.setText(null);
			gameShow.panel.requestFocus();
		}
		if( chatShow !=null &&  e.getComponent() == chatShow.check)
		{
			if(chatShow.check.isSelected()==true)
			{	
				clientShowProtocol.isRecord = true;
				chatShow.isCheck = true;
			}
			else
			{
				clientShowProtocol.isRecord = false;
				chatShow.isCheck = false;
			}
		}		
	}
	public void mousePressed(MouseEvent e)
	{
		if(e.getComponent() == gameShow.panel)
		{
			socket.out("rotate-to "+e.getX()+" "+e.getY()+" ");
			socket.out("shoot ");
			socket.out("move-to "+e.getX()+" "+e.getY()+" ");
		}
	}
}
