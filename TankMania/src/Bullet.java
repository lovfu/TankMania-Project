/*
 *	TankMania/Bullet.java
 *
 *  TankMania/Bullet.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

public class Bullet
{
	public final int speed =5;
	public int ownerID;
	public int sx;
	public int sy;
	public int x;
	public int y;
	public int angle;
	public boolean isShow;
	public final int lastDistance = 400;
	public static ServerAllSocket socket;
	
	public void destroy()
	{
		isShow = false;
		ownerID = -1;
	}
	public Bullet()
	{
		isShow = false;
		ownerID = -1;
	}
	public void set(int ownerID,int x,int y,int angle)
	{
		this.angle = angle;
		this.x = x;
		this.y = y;
		this.ownerID = ownerID;
		this.sx = x;
		this.sy = y;
		isShow = true;
	}
	public void step(int id)
	{
		if(isShow)
		{
			x = x + (int)(speed*Math.cos(angle/180.0*Math.PI));
			y = y + (int)(speed*Math.sin(angle/180.0*Math.PI));
			if(x < 5 || y < 5 || x > 590 || y > 390)
			{
				destroy();
				socket.spread("destroy bullet "+id);
			}
			if(Math.abs((x-sx)*(x-sx)+(y-sy)*(y-sy))>lastDistance*lastDistance)
			{
				destroy();
				socket.spread("destroy bullet "+id);
			}
		}
	}
	public void move(int x,int y)
	{
		this.x = x;
		this.y = y;
	}
	public void setAngle(int angle)
	{
		this.angle = angle;
	}
	public void hit(ServerData data,int chatid,int id)
	{
		if(isShow)
		{
			Tank tank;
			Chat chat = data.chat[chatid];
			for(int i = 0;i < chat.tankNum;i++)
			{
				tank = data.tank[chat.tankID[i]];
				if(ownerID != chat.tankID[i] &&((tank.x-x)*(tank.x-x)+(tank.y-y)*(tank.y-y))<400)
				{
					tank.isHited = true;
					tank.life = tank.life - 5;
					if(tank.life > 0)
						socket.spread("set-life tank "+chat.tankID[i]+" "+tank.life);
					else if(tank.life == 0)
						socket.spread("destroy tank "+chat.tankID[i]);
					destroy();
					socket.spread("destroy bullet "+id);
				}
			}
		}
	}
}
