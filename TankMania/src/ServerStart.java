/*
 *	TankMania/ServerStart.java
 *
 *  TankMania/ServerStart.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 *	ServerAllSocket
 * 	ServerLogicThread
 * 	
 */

import java.net.*;
class  ServerStart
{
	private ServerSocket serverSocket;
	private Socket tempSocket = new Socket();
	public static ServerAllSocket serverAllSocket = new ServerAllSocket();
	public static ServerLogicThread serverLogic;
	
	public static void main(String args[])
	{
		new ServerStart(4800);
	}
	public ServerStart(int port)
	{
		try
		{
			serverSocket = new ServerSocket(port);		//ServerSocket	Init
		}
		catch(Exception e)
		{
			System.out.println("ServerSocket Init Error ! Exit!");
			System.exit(0);
		}
		serverLogic = new ServerLogicThread();		
		serverLogic.start();						//Logic Thread Start
		
		while(true)
		{
			try
			{
				tempSocket = serverSocket.accept();
				serverAllSocket.setSocket(tempSocket);
			} catch(Exception e)
			{
				System.out.println("Logic:Error!");
				System.exit(0);
			}
		}	
	}
}
