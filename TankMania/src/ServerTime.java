/*
 *	TankMania/ServerTime.java
 *
 *  TankMania/ServerTime.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

public class ServerTime
{
	public static int time;
	public static int waitRound;
	public static int unitRound;
	public static ServerAllSocket socket;
	
	public ServerTime()
	{
		time = 50;
		unitRound = 10;
	}
	public static void go()
	{
		time = time + unitRound;
	}
	public static void update()
	{
		waitRound = unitRound * socket.socketNum;
	}
}
