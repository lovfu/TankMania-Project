/*
 *	TankMania/ClientConnectErrorDialog.java
 *
 *  TankMania/ClientConnectErrorDialog.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.*;

public class ClientConnectErrorDialog extends MouseAdapter implements KeyListener
{
	private JFrame connectErrorDialog;
	private JLabel infoLabel;
	private JButton enterButton;
	private Font font;

	public ClientConnectErrorDialog()
	{
		connectErrorDialog = new JFrame("Connect Error !");
		infoLabel = new JLabel(" Connect Error！");
		enterButton = new JButton("Enter");
		font = new Font("DialogInput",font.BOLD,20);

		connectErrorDialog.setSize(300,200);
		int x = WindowLocation.getX(connectErrorDialog.getWidth());
		int y = WindowLocation.getY(connectErrorDialog.getHeight());

		connectErrorDialog.getContentPane().add(infoLabel);
		connectErrorDialog.getContentPane().add(enterButton);
		connectErrorDialog.setLocation(x, y);
		connectErrorDialog.setResizable(false);
		connectErrorDialog.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
		connectErrorDialog.setLayout(null);
		connectErrorDialog.setVisible(true);

		infoLabel.setFont(font);
		infoLabel.setBounds(50,20,200,50);
		enterButton.setBounds(100,90,100,50);
		enterButton.setMnemonic(KeyEvent.VK_ENTER);
		enterButton.addMouseListener(this);
		enterButton.addKeyListener(this);
	}

	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent() == enterButton)
			System.exit(0);
	}
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode()==KeyEvent.VK_ENTER)
			System.exit(0);
	}
	public void keyReleased(KeyEvent e){}
	public void keyTyped(KeyEvent e){}
}
