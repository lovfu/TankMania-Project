/*
 *	TankMania/ClientWaitShow.java
 *
 *  TankMania/ClientWaitShow.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 * 	ClientData
 * 	
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class ClientWaitShow implements ActionListener
{
	public static JFrame frame;
	public static JButton chat;
	public static JButton exit;
	public static JButton join;
	public static JList	list;
	public static JLabel label;
	public static BufferedImage img;
	public static JScrollPane scrollPane;
	public static String info[] = new String[27];
	public static ClientData data;
	private static Timer time;
	public static int selected;
	
	public ClientWaitShow()
	{
		File sourceImage = new File("light.png");
		try{
			img = ImageIO.read(sourceImage);
		}catch(Exception e){
			System.out.println("Error!");
			System.exit(0);
		}
		time = new Timer(1000,this);
		frame = new JFrame("Tank Mania-Waiting Room");
		chat =  new JButton("Create Game");
		join = new JButton("Join Game");
		exit = new JButton("Exit");
		list = new JList(info);
		label = new JLabel(new ImageIcon(img));
		scrollPane = new JScrollPane(list,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		frame.setResizable(false);
		frame.setLayout(null);
		frame.getContentPane().add(scrollPane);
		
		frame.getContentPane().add(label);
		frame.getContentPane().add(chat);
		frame.getContentPane().add(join);
		frame.getContentPane().add(exit);

		label.setBounds(0,0,img.getWidth(),img.getHeight());
		scrollPane.setBounds(img.getWidth(),0,WindowLocation.getWidth()*5/8-img.getWidth()-2,WindowLocation.getHeight()/2-80);
		chat.setBounds(img.getWidth()+10,WindowLocation.getHeight()/2-70,140,30);
		join.setBounds(img.getWidth()+160,WindowLocation.getHeight()/2-70,140,30);
		exit.setBounds(img.getWidth()+310,WindowLocation.getHeight()/2-70,140,30);
	
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(false);
		frame.setSize(WindowLocation.getWidth()*5/8,WindowLocation.getHeight()/2);
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);
	
		frame.addWindowListener(ClientStart.clientRequest);
		chat.addMouseListener(ClientStart.clientRequest);
		join.addMouseListener(ClientStart.clientRequest);
		exit.addMouseListener(ClientStart.clientRequest);
		list.addMouseListener(ClientStart.clientRequest);
		list.addKeyListener(ClientStart.clientRequest);
		chat.addKeyListener(ClientStart.clientRequest);
		join.addKeyListener(ClientStart.clientRequest);
		exit.addKeyListener(ClientStart.clientRequest);
	}
	public static void show()
	{
		time.start();
		frame.setVisible(true);
	}
	public static void close()
	{
		time.stop();
		frame.setVisible(false);
	}
	public void actionPerformed(ActionEvent e)
	{
		data.updateWait();
		list.setListData(data.chatInfo);
		list.setSelectedIndex(selected);
	}	
}
