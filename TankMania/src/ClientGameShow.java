/*
 *	TankMania/ClientGameShow.java
 *
 *  TankMania/ClientGameShow.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 * ClientData
 * 
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class ClientGameShow
{
	public static JFrame frame;
	public static GPanel panel;
	public static JTextArea show;
	public static JTextField enterText;
	public static JButton enter;
	public static JButton exit;
	public static JScrollPane scrollPane;	
	public static ClientData data;
	public static Font font;
	
	public ClientGameShow()
	{
		font = new Font("DialogInput",Font.BOLD,15);
		frame = new JFrame("TankMania-Game Room");
		exit = new JButton("Leave");
		enter = new JButton("Enter");
		panel = new GPanel(data.chat[data.tank[data.myID].chatID]);
		enterText = new JTextField();
		
		frame.add(panel);		
		frame.getContentPane().add(exit);
		frame.getContentPane().add(enter);
		frame.getContentPane().add(enterText);
		
		frame.setResizable(false);
		frame.setVisible(false);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(600,462);	
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);		
		panel.setLocation(0,0);
		enterText.setBounds(0,401,435,30);
		enterText.setFont(font);
		enter.setBounds(435,401,80,29);
		exit.setBounds(517,401,80,29);
		
		frame.addWindowListener(ClientStart.clientRequest);
		panel.addKeyListener(ClientStart.clientRequest);
		enter.addKeyListener(ClientStart.clientRequest);
		exit.addKeyListener(ClientStart.clientRequest);
		enterText.addMouseListener(ClientStart.clientRequest);
		enterText.addKeyListener(ClientStart.clientRequest);
		panel.addMouseListener(ClientStart.clientRequest);
		enter.addMouseListener(ClientStart.clientRequest);
		exit.addMouseListener(ClientStart.clientRequest);
	}
	public static void close()
	{
		frame.setVisible(false);
		panel.timer.stop();
	}
	public static boolean getOpened()
	{
		return frame.isVisible();
	}
	public static void create()
	{
		frame.setVisible(true);
		panel.start();
		panel.requestFocus();
	}
}
