/*
 *	TankMania/ServerLogicThread.java
 *
 *  TankMania/ServerLogicThread.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 * 	ServerAllSocket
 *	ServerTime
 * 	ServerProtocol
 * 
 */

import java.net.*;
import java.io.*;

public class ServerLogicThread extends Thread
{
	public static ServerTime time;
	public static ServerAllSocket socket;
	public static ServerProtocol serverProtocol;
	public ServerLogicThread()
	{
		time = new ServerTime();
		socket = new ServerAllSocket();
		serverProtocol = new ServerProtocol();
	}
	public void run()
	{
		int i;
		int t;
		while(true)
		{
			t = 0;
			for(i = 0;i < 100;i++)
			{
				if(socket.getConnect(i))		//isConnected
				{
					t++;
					socket.sendTime(i);			//Sync Time
					serverProtocol.deal(i);		//	Socket Recieve and Deal
					try
					{
						time.go();	//Time System	
						sleep(time.unitRound);	//Time	Control
					}catch(Exception e)
					{	
						System.out.println("Sleep Error!");
						System.exit(0);	
					}
				}		
			}
			if(t == 0)
			{
				try
				{
					time.go();	//Time System	
					sleep(time.unitRound);	//Time	Control
				}catch(Exception e)
				{	
					System.out.println("Sleep Error!");
					System.exit(0);	
				}
			}
		}
	}
}
