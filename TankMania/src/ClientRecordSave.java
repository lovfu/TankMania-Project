/*
 *	TankMania/ClientRecordSave.java
 *
 *  TankMania/ClientRecordSave.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */
import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.awt.event.*;

public class ClientRecordSave implements MouseListener,KeyListener
{
	public static JFrame frame;
	public static JTextField reader;
	public static RecordFileSave fileReader;
	public static JLabel label;
	public static JButton enter;
	public static JButton choose;
	public static JButton exit;
	private static Font font;
	public static File file;
	public static String directory;
	public static File record;
	public static ClientWaitShow waitShow;
	public static ClientShowProtocol clientShowProtocol;
	public ClientRecordSave()
	{
		frame = new JFrame("TankMania Record Save");
		reader = new JTextField();
		label = new JLabel("Please Save The Record File");
		exit = new JButton("Exit");
		enter = new JButton("Enter");
		choose = new JButton("Save");
		font = new Font("DialogInput",Font.BOLD,15);
		record = new File(System.getProperty("user.dir"),"record.txt");
		
		fileReader = new RecordFileSave();
		frame.getContentPane().add(reader);
		frame.getContentPane().add(label);
		frame.getContentPane().add(choose);
		frame.getContentPane().add(enter);
		frame.getContentPane().add(exit);
		frame.setResizable(false);
		frame.setLayout(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(WindowLocation.getWidth()/3,WindowLocation.getHeight()/3);
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);
		reader.setEditable(false);
		
		label.setFont(font);
		label.setBounds(70,30,300,50);
		reader.setBounds(50,80,300,30);
		choose.setBounds(50,130,80,30);
		enter.setBounds(150,130,80,30);
		exit.setBounds(250,130,80,30);
		
		choose.addMouseListener(this);
		enter.addMouseListener(this);
		exit.addMouseListener(this);
		choose.addKeyListener(this);
		enter.addKeyListener(this);
		exit.addKeyListener(this);
	}
	public void mouseExited(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseEntered(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void keyReleased(KeyEvent e){}
	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == exit)
		{	
			frame.setVisible(false);
			waitShow.show();
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == choose)
		{
			frame.setVisible(false);
			fileReader.setVisible(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource() == enter)
			enter();
	}
	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent() == exit)
		{	
			frame.setVisible(false);
			waitShow.show();
		}
		if(e.getComponent() == choose)
		{
			frame.setVisible(false);
			fileReader.frame.setVisible(true);
		}
		if(e.getComponent() == enter)
			enter();
	}
	public static void enter()
	{			
		if(reader.getText().trim().length() > 0)
		{
			clientShowProtocol.output.close();
			BufferedReader input;
			PrintStream output;
			try
			{	
				input = new BufferedReader(new FileReader(record));
				output = new PrintStream(file);
				String str;
				do
				{
					str = input.readLine();
					output.println(str);
				}while(!str.equals("end"));	
				input.close();
				output.close();
				clientShowProtocol.output=new PrintStream(record);
			}
			catch(Exception ex)
			{
				System.out.println("BufferedReader Record Init Error!");
				System.exit(0);
			}
			frame.setVisible(false);
			waitShow.show();
		}
	}
}

class RecordFileSave extends JFileChooser
{
	public JFrame frame;
	public String str;
	public ClientRecordSave reader;
	public RecordFileSave()
	{
		super(System.getProperty("user.dir"));
		frame = new JFrame("TankMania Record Save");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		this.setDialogType(JFileChooser.SAVE_DIALOG);
		frame.setVisible(false);
		frame.setSize(WindowLocation.getWidth()/2,WindowLocation.getHeight()/2);
		int x=WindowLocation.getX(frame.getWidth());
		int y=WindowLocation.getY(frame.getHeight());
		frame.setLocation(x,y);
		frame.getContentPane().add(this);
	}
	public void approveSelection()
	{
		reader.file = getSelectedFile();
		if(reader.file.exists()) 
		{
			int i = javax.swing.JOptionPane.showConfirmDialog(this, "该文件已经存在，确定要覆盖吗？");
			if(i != javax.swing.JOptionPane.YES_OPTION)
				return;
		}
		try
		{
			reader.file.createNewFile();
		}catch(Exception e)
		{
			System.out.println("Save File Error");
		}
		reader.reader.setText(reader.file.toString());
		frame.setVisible(false);
		reader.frame.setVisible(true);
	}
	public void cancelSelection() 
	{
		frame.setVisible(false);
		reader.frame.setVisible(true);
	}
}
