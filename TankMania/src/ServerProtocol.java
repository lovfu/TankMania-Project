/*
 *	TankMania/ServerRecieveProtocol.java
 *
 *  TankMania/ServerRecieveProtocol.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

/*
 * 	ServerAllSocket
 * 	ServerTime
 * 	ServerData
 * 
 */
import java.io.*;
import java.net.*;

public class ServerProtocol
{
	public static int chatID;
	private static int i;
	public static String tag1;
	public static String tag2;
	public static String tankName;
	public static String chatString;
	private static String str ;
	private static String temp ;
	public static int tankX;
	public static int tankY;
	public static int gunX;
	public static int gunY;
	public static ServerAllSocket socket;
	public static ServerTime time;
	public static ServerData data;
	
	public ServerProtocol()
	{
		socket  = new  ServerAllSocket();
		time = new ServerTime();
		data = new ServerData();
		tankName = new String();
		chatString = new String();
	}
	
	public static void deal(int tankID)
	{
		try{
			str = new String(socket.is[tankID].readLine());
			if(!str.equals("sync "))
				System.out.println("Recieve:"+str);
		}
		catch(Exception e) 
		{	
			System.out.println("Read Error!");
			System.exit(0);	
		}
		tag1 = new String(dealWithString());
		if(tag1.equals("sync"))
			socket.sendTime(tankID);		
		if(tag1.equals("exit"))
		{
			data.tank[tankID].isConnected = false;
			socket.isConnected[tankID] = false;
			try 
			{
				socket.socket[tankID].close();
				socket.socket[tankID] = new Socket();
				
			}catch(Exception e)
			{
				System.out.println("Socket Close Error!");
				System.exit(0);
			}
			return;
		}
		if(tag1.equals("shoot"))
		{
			Tank temp = data.tank[tankID];
			int t = data.getFirstBullet(temp.chatID);
			data.bullet[temp.chatID][t].set(tankID,temp.x,temp.y,temp.gunAngle);
			socket.spread("create bullet "+t+" 0 "+tankID +" "+data.bullet[temp.chatID][t].x+" "+data.bullet[temp.chatID][t].y+" 0 ");
		}
		if(tag1.equals("move-to"))
		{
			tankX = Integer.parseInt(dealWithString());
			tankY = Integer.parseInt(dealWithString());
			Tank temp = data.tank[tankID];
			int t =(int)(Math.atan((double)(tankY-temp.y)/(double)(tankX-temp.x))/Math.PI*180);;
			if(tankX-temp.x<=0) t = t + 180;
			socket.spread("rotate-tank tank "+tankID+" "+data.tank[tankID].angle+" "+t+" 0");
			data.tank[tankID].angle = t;
			System.out.println(t);
		}
		if(tag1.equals("rotate-to"))
		{
			Tank temp = data.tank[tankID];
			gunX = Integer.parseInt(dealWithString());
			gunY = Integer.parseInt(dealWithString());
			int t =(int)(Math.atan((double)(gunY-temp.y)/(double)(gunX-temp.x))/Math.PI*180);;
			if(gunX-temp.x<=0)
				t = t + 180;
			socket.spread("rotate-gun tank "+tankID+" "+temp.gunAngle+" "+t+" "+0);	
			temp.gunAngle = t;
		}
		if(tag1.equals("leave"))
		{
			chatID = data.tank[tankID].chatID;
			tag2 = dealWithString();
			if(tag1.equals("leave") && tag2.equals("chat"))
			{
				if(data.chat[chatID].ownerID == tankID)
					socket.spread(new String("destroy chat "+chatID));
				else
				{
					socket.spread(new String("destroy chater "+chatID+" "+tankID));
					socket.spread(new String("update chat "+chatID+" "+data.chat[chatID].tankNum));
				}
				data.leaveChat(chatID,tankID);
			}
			if(tag1.equals("leave") && tag2.equals("game"))
			{
				socket.spread(new String("destroy chater "+chatID+" "+tankID));
				socket.spread(new String("update game "+chatID+" "+data.chat[chatID].tankNum));
				if(data.chat[chatID].tankNum == 1) 
				{	
					socket.spread(new String("destroy game "+chatID));
					data.destroyBullet(chatID);
				}
				data.leaveGame(chatID,tankID);
			}
		}

		if(tag1.equals("born"))
		{
			tankName = dealWithString();
			dealWithString();
			dealWithString();
			dealWithString();
			dealWithString();
			socket.send(tankID,"set-id "+tankID);
			for(i = 0;i < 25;i++)
				if(data.chat[i].isChat)
					socket.send(tankID,"create chat "+i+" "+data.chat[i].ownerID+" "+data.tank[data.chat[i].ownerID].name+" "+data.chat[i].tankNum); 
			for(i = 0;i < 25;i++)
				if(data.chat[i].isChat && data.chat[i].isGame)
					socket.send(tankID,"create game "+i); 		
			for(i = 0;i < 100;i++)
				if(socket.getConnect(i) && data.tank[i].chatID !=-1 && data.chat[data.tank[i].chatID].ownerID != data.tank[i].chatID)
					socket.send(tankID,"create chater "+data.tank[i].chatID+" "+i+" "+data.tank[i].name);
			data.tank[tankID].init(tankName);
		}
		if(tag1.equals("create"))
		{
			tag2 = new String(dealWithString());	
			if(tag1.equals("create") && tag2.equals("chat"))
			{
				int t = data.getFirstChat();
				socket.spread("create chat "+t+" "+tankID+" "+data.tank[tankID].name);
				data.createChat(t,tankID);
			}
			if(tag1.equals("create") && tag2.equals("game"))
			{
				chatID = data.tank[tankID].chatID;
				data.initGame(chatID);
				int i = 0;
				for(i = 0;i<data.chat[chatID].tankNum;i++)
					socket.spread("update chater "+chatID+" "+data.chat[chatID].tankID[i]+" "+i+" "+data.tank[data.chat[chatID].tankID[i]].name);
				i = 0;
				socket.spread("create game "+data.tank[tankID].chatID);
				socket.spread("update chat "+chatID+" "+data.chat[chatID].tankNum);
				while(i < data.chat[chatID].tankNum)
				{
					Tank temp = data.tank[data.chat[chatID].tankID[i]];
					socket.spreadTank(temp,data.chat[chatID].tankID[i]);
					i++;
				}
				data.chat[data.tank[tankID].chatID].isGame = true;
			}
		}
		if(tag1.equals("join"))
		{
			tag2 = dealWithString();
			if(tag1.equals("join") && tag2.equals("chat"))
			{
				chatID = Integer.parseInt(dealWithString());
				socket.spread("create chater "+chatID+" "+tankID+" "+data.tank[tankID].name);
				data.joinChat(chatID,tankID);	
			}
		}
		if(tag1.equals("chat"))
		{
			tag2 = dealWithString();
			if(tag1.equals("chat") && tag2.equals("chat"))
			{
				chatString = new String(str);
				socket.spread("chat chat "+data.tank[tankID].chatID+" "+tankID+" "+chatString);
			}
		}
		if(tag1.equals("game"))
		{
			tag2 = dealWithString();
			if(tag1.equals("game") && tag2.equals("chat"))
			{
				chatString = new String(str);
				socket.spread("game chat "+data.tank[tankID].chatID+" "+tankID+" "+chatString);
			}
		}
		update(tankID);	
	}
	private static String dealWithString()
	{
		i = str.indexOf(32);
		temp = new String(str.substring(0,i));
		str =  new String(str.substring(i+1));
		return temp;
	}
	public static void update(int tankID)
	{
		if(data.tank[tankID].chatID != -1 && data.chat[data.tank[tankID].chatID].isGame)
		{
			int chatID = data.tank[tankID].chatID;
			data.updateGame(chatID);
			for(int i = 0;i < 100;i++)
				if(data.bullet[chatID][i].isShow && data.tank[tankID].isConnected) 
					socket.send(tankID,"move bullet "+i+" 0 0 "+data.bullet[chatID][i].x+" "+data.bullet[chatID][i].y+" 0 ");
			for(int i = 0;i < 100;i++)
				if(data.tank[i].chatID == chatID)
				{
					if(data.tankHit(i,data.tank[i].x,data.tank[i].y))
						socket.send(tankID,"move tank "+i+" 0 0 "+data.tank[i].x+" "+data.tank[i].y+" 0 ");
				}	
		}
	}
}
