/*
 *	TankMania/ClientTime.java
 *
 *  TankMania/ClientTime.class
 *
 * 	(C) 王宏伟 2008010486 计84 清华大学
 * 
 */

public class ClientTime
{
	private static int time;
	private static int round;
	private static int lastRequestTime = -1000;
	public static int chatRound = 4000;
	public static int hitRound = 1000;
	public static int updateRound = 100;
	
	public static int getLast()
	{
		return lastRequestTime;
	}
	public static void setLast()
	{
		lastRequestTime = time;
	}
	public static void update(int i,int j)
	{
		time = i;
		round = j;
	}

	public static int getRound()
	{
		return round;
	}
	public static int getTime()
	{
		return time;
	}
}
