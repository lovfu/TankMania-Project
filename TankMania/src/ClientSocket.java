/*
 *	TankMania/StartTankMania.java
 *
 *  TankMania/StartTankMania.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */
import java.io.*;
import java.net.*;

public class ClientSocket
{
	public static Socket socket;
	public static PrintWriter os;
	public static BufferedReader is;
	public static ClientTime time;
	public static boolean isRequest;
	public static List list;
	public static String request;
	
	public ClientSocket()
	{
		list = new List();
		isRequest = false;
		try
		{
			os = new PrintWriter(socket.getOutputStream());
			is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		}
		catch(Exception e)
		{
			System.out.println("IO Init Error!");
			System.exit(0);
		}	
	}
	public static void go()
	{
		try
		{
			socket = new  Socket("127.0.0.1",4800);
		}
		catch(Exception e)
		{
			new ClientConnectErrorDialog();
		}		
	}
	public static void out(String str)
	{
		list.put(str);
	}
	public static void output()
	{
		if((request=list.get())==null)
			request = new String("sync ");
		else
			System.out.println("send:"+request);
		os.println(request);
		os.flush();
		time.setLast();
	}
	public static void exit()
	{
		os.println("exit ");
		os.flush();
		System.exit(0);
	}
}

class List
{
  private static String list[];
  private static int length;
  private static int tail;//队尾的地址
  private static int head;//队头的地址
  
  List()
  {
    length=100;
    tail=-1;//表示不存在元素，即不存在队尾
    head=-1;//表示不存在元素，即不存在队头
    list=new String[100];
  }
  
  public static void put(String str)
  {
    if(head==tail+1||(tail==head+length-1))
      return;
    if(tail<length-1) tail++;
    else tail=0;
    if(head==-1)//考虑由不存在元素转变为存在元素之间的过程
      head=0;
    list[tail]=new String(str);
  }
  
  public static String get()
  {
    if(tail==-1)//不存在元素，即tail和head都为-1
      return null;
    String c=list[head];
    if(tail==head)
    {
      tail=-1;
      head=-1;
      return c;
    }//考虑即将过渡到不存在元素时的情形。
    if(head<length-1) head++;
    else head=0;
    return c;
  }
}
