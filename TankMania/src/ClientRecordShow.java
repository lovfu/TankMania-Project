/*
 *	TankMania/ClientRecordShow.java
 *
 *  TankMania/ClientRecordShow.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */

import java.io.*;
import java.awt.event.*;
import javax.swing.*;

public class ClientRecordShow implements ActionListener
{
	public static File file;
	public static BufferedReader input;
	public static ClientShowProtocol clientShowProtocol;
	public static String str;
	public static String tag;
	public static ClientWaitShow waitShow;
	public static ClientChatShow chatShow;
	public static ClientGameShow gameShow;
	public static ClientTime time;
	public static Timer timer;
	public static ClientData data;
	
	public ClientRecordShow(String s)
	{
		data = new ClientData();
		new ClientTime();
		clientShowProtocol = new ClientShowProtocol(false);
		file = new File(s);
		try
		{	
			input = new BufferedReader(new FileReader(file));
		}
		catch(Exception e)
		{
			System.out.println("BufferedReader Record Init Error!");
			System.exit(0);
		}
		timer = new Timer(10,this);
		timer.start();
	}
	
	public void actionPerformed(ActionEvent e)
	{
		
		if(time.getTime() >= time.getLast() + time.getRound())
		{
			try
			{	
				str = new String(input.readLine());
			}
			catch(Exception ex)
			{
				System.out.println("BufferedReader Record Read Error!");
				System.exit(0);
			}
			if(!str.equals("end"))
				clientShowProtocol.deal(str);
			else
			{
				waitShow.close();
				timer.stop();
			}
		}
	}
}
