/*
 *	TankMania/WindowLocation.java
 *
 *  TankMania/WindowLocation.class
 *
 * (C) 王宏伟 2008010486 计84 清华大学
 * 
 */
import java.awt.Toolkit;

public class WindowLocation
{
	static Toolkit tk = Toolkit.getDefaultToolkit();
	static int getX(int w)
	{
		return (tk.getDefaultToolkit().getScreenSize().width - w)/2;	
	}
	
	static int getY(int h)
	{
		return (tk.getDefaultToolkit().getScreenSize().height - h)/2;
	}
	static int getWidth()
	{
		return tk.getDefaultToolkit().getScreenSize().width;
	}
	static int getHeight()
	{
		return tk.getDefaultToolkit().getScreenSize().height;
	}
}
